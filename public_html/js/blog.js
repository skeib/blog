$(function () {
    var APPLICATION_ID = "56F62848-D35A-0D8A-FF90-A2ABCB906300",
        SECRET_KEY = "0F5F61EC-000A-94CC-FF38-ACFD80959200",
        VERSION = "v1";

    Backendless.initApp(APPLICATION_ID, SECRET_KEY, VERSION);
    
    
    var postsCollection = Backendless.Persistence.of(Posts).find();
    
    console.log(postsCollection);
    
    var wrapper = {
        posts: postsCollection.data
    };
    console.log(wrapper);
    var d = new Date();
    var now = d.getTime();
    var day = 24 * 60 * 60 * 1000;
    
    var length = postsCollection.data.length;
    var a = 0;
    
    var counter = 0;
    for (i = 0; i < length; i++)
    {
        a = postsCollection.data[i].created;
        if(now - a < day){
            counter++;
        }
    }
    Handlebars.registerHelper('format', function (time){
        return moment (time).format("dddd, MMMM, Do, YYYY");
    });
    
    var blogScript = $("#blogs-template").html();
    var blogTemplate = Handlebars.compile(blogScript);
    var blogHTML = blogTemplate(wrapper);
    
    $('.main-container').html(blogHTML);
    $('.badge').html(counter);

});

function Posts (args) {
    args = args || {};
    this.title = args.title || "";
    this.content = args.content || "";
    this.authorEmail = args.authorEmail || "";
}



